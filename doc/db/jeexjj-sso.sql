-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: jeexjj_sso
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_sec_menu`
--

DROP TABLE IF EXISTS `t_sec_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sec_menu` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(2000) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `PRIVILEGE_CODE` varchar(200) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `order_sn` int(11) DEFAULT NULL,
  `ICON` varchar(100) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `code` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `unique_code` (`code`),
  KEY `FK_psv09uu72jp7gwbch9sgm0pm6` (`PARENT_ID`),
  CONSTRAINT `FK_psv09uu72jp7gwbch9sgm0pm6` FOREIGN KEY (`PARENT_ID`) REFERENCES `t_sec_menu` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8 COMMENT='菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sec_menu`
--

LOCK TABLES `t_sec_menu` WRITE;
/*!40000 ALTER TABLE `t_sec_menu` DISABLE KEYS */;
INSERT INTO `t_sec_menu` VALUES (153,'权限管理','权限管理',NULL,'',NULL,2,'users','valid','02'),(155,'系统管理','系统管理',NULL,'',NULL,1,'desktop','valid','01'),(156,'字典管理','字典管理',155,'sys_dict','/sys/dict/index',1,NULL,'valid','0101'),(158,'管理员管理','管理员管理',153,'sec_manager','/sec/manager/index',NULL,NULL,'valid','0202'),(159,'用户管理','用户管理',153,'sec_user','/sec/user/index',NULL,NULL,'valid','0203'),(161,'菜单管理','菜单管理',153,'sec_menu','/sec/menu/index',NULL,NULL,'valid','0204'),(162,'角色管理','菜单管理',153,'sec_role','/sec/role/index',NULL,NULL,'valid','0201'),(163,'文件管理','文件管理',155,'sys_xfile','/sys/xfile/index',NULL,NULL,'valid','0103'),(164,'项目管理','项目管理',155,'xsso_project','/xsso/project/index',NULL,NULL,'valid','0104'),(165,'最近在线用户','最近在线用户',155,'xsso_ticket','/xsso/ticket/index',NULL,NULL,'valid','0105');
/*!40000 ALTER TABLE `t_sec_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sec_role`
--

DROP TABLE IF EXISTS `t_sec_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sec_role` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(200) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(200) DEFAULT NULL COMMENT '编码',
  `DESCRIPTION` varchar(2000) DEFAULT NULL COMMENT '描述',
  `status` varchar(20) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_code` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sec_role`
--

LOCK TABLES `t_sec_role` WRITE;
/*!40000 ALTER TABLE `t_sec_role` DISABLE KEYS */;
INSERT INTO `t_sec_role` VALUES (4,'管理员','admin','管理员','valid'),(5,'录入员','lulu','录入员','valid'),(6,'教师','teacher','教师','valid'),(7,'班主任','banzhren','班主任','valid');
/*!40000 ALTER TABLE `t_sec_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sec_role_privilege`
--

DROP TABLE IF EXISTS `t_sec_role_privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sec_role_privilege` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `PRIVILEGE_TITLE` varchar(200) DEFAULT NULL,
  `PRIVILEGE_CODE` varchar(100) DEFAULT NULL,
  `FUNCTION_LIST` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_37slprxb3e2ygav598e3yeawt` (`ROLE_ID`),
  CONSTRAINT `FK_37slprxb3e2ygav598e3yeawt` FOREIGN KEY (`ROLE_ID`) REFERENCES `t_sec_role` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sec_role_privilege`
--

LOCK TABLES `t_sec_role_privilege` WRITE;
/*!40000 ALTER TABLE `t_sec_role_privilege` DISABLE KEYS */;
INSERT INTO `t_sec_role_privilege` VALUES (14,4,'代码生成管理','sys_code','generate'),(15,4,'菜单管理','sec_menu','edit|delete|create|list'),(20,4,'字典管理','sys_dict','edit|delete|create|list'),(26,4,'管理员管理','sec_manager','delete|create|list|edit'),(27,4,'角色管理','sec_role','edit|delete|create|list'),(28,4,'用户管理','sec_user','edit|delete|create|list'),(29,5,'菜单管理','sec_menu','edit|delete|create|list'),(30,6,'角色管理','sec_role','edit|delete|create|list'),(31,6,'文件管理','sys_xfile','edit|delete|create|list'),(32,6,'用户管理','sec_user','edit|deletelist');
/*!40000 ALTER TABLE `t_sec_role_privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sec_user`
--

DROP TABLE IF EXISTS `t_sec_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sec_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_type` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `province` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_login_name` (`login_name`) USING BTREE,
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sec_user`
--

LOCK TABLES `t_sec_user` WRITE;
/*!40000 ALTER TABLE `t_sec_user` DISABLE KEYS */;
INSERT INTO `t_sec_user` VALUES (52,'admin','e10adc3949ba59abbe56e057f20f883e','系统管理员','admin','status','12345678911',NULL,'2018-04-19 13:30:57','valid','2018-04-19 00:00:00',NULL),(54,'zhanghejie','e10adc3949ba59abbe56e057f20f883e','张合杰','user','','13366442927',NULL,'2018-04-27 16:16:38','valid','2013-03-07 00:00:00',NULL),(58,'tutor','e10adc3949ba59abbe56e057f20f883e','教师','admin','jlsdzhj@126.com','13366442928',NULL,'2018-05-03 17:40:45','valid','2018-05-03 00:00:00',NULL),(76,'test3','e10adc3949ba59abbe56e057f20f883e','张三3','user','zhangsan@128.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(77,'test4','e10adc3949ba59abbe56e057f20f883e','张三4','user','zhangsan@129.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(78,'test5','e10adc3949ba59abbe56e057f20f883e','张三5','user','zhangsan@130.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(79,'test6','e10adc3949ba59abbe56e057f20f883e','张三6','user','zhangsan@131.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(80,'test7','e10adc3949ba59abbe56e057f20f883e','张三7','user','zhangsan@132.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(81,'test8','e10adc3949ba59abbe56e057f20f883e','张三8','user','zhangsan@133.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(82,'test9','e10adc3949ba59abbe56e057f20f883e','张三9','user','zhangsan@134.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(83,'test10','e10adc3949ba59abbe56e057f20f883e','张三10','user','zhangsan@135.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(84,'test11','e10adc3949ba59abbe56e057f20f883e','张三11','user','zhangsan@136.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(85,'test12','e10adc3949ba59abbe56e057f20f883e','张三12','user','zhangsan@137.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(86,'test13','e10adc3949ba59abbe56e057f20f883e','张三13','user','zhangsan@138.com',NULL,NULL,'2018-05-07 14:42:55','valid',NULL,NULL),(87,'bbt','e10adc3949ba59abbe56e057f20f883e','bbt','user','bbt','bbt',NULL,'2018-05-08 14:29:20','valid','2018-05-08 00:00:00',NULL),(88,'shandongren','e10adc3949ba59abbe56e057f20f883e','shandongren','user','shandongren','shandongren',NULL,'2018-05-08 14:50:41','valid','2018-05-08 00:00:00','henan');
/*!40000 ALTER TABLE `t_sec_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sec_user_role`
--

DROP TABLE IF EXISTS `t_sec_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sec_user_role` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sec_user_role`
--

LOCK TABLES `t_sec_user_role` WRITE;
/*!40000 ALTER TABLE `t_sec_user_role` DISABLE KEYS */;
INSERT INTO `t_sec_user_role` VALUES (1,58,6);
/*!40000 ALTER TABLE `t_sec_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sso_project`
--

DROP TABLE IF EXISTS `t_sso_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sso_project` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) DEFAULT NULL,
  `CODE` varchar(200) NOT NULL,
  `CREATE_USER_ID` bigint(20) DEFAULT NULL,
  `CREATE_USER_NAME` varchar(200) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `LOGIN_URL` varchar(1000) DEFAULT NULL,
  `VALID_IP` varchar(1000) DEFAULT NULL,
  `PWD_TYPE` varchar(45) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE KEY `unique_project_code` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sso_project`
--

LOCK TABLES `t_sso_project` WRITE;
/*!40000 ALTER TABLE `t_sso_project` DISABLE KEYS */;
INSERT INTO `t_sso_project` VALUES (36,'csdn','csdn',52,'系统管理员','2018-05-09 14:24:26','http://www.csdn.net','','md5','valid'),(37,'百度','baidu',52,'系统管理员','2018-05-09 14:29:32','https://www.baidu.com','','','valid'),(38,'zhj','zhj',52,'系统管理员','2018-05-12 10:03:21','','','','valid');
/*!40000 ALTER TABLE `t_sso_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sys_dict`
--

DROP TABLE IF EXISTS `t_sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sys_dict` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `group_code` varchar(16) NOT NULL,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `sn` int(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='字典';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sys_dict`
--

LOCK TABLES `t_sys_dict` WRITE;
/*!40000 ALTER TABLE `t_sys_dict` DISABLE KEYS */;
INSERT INTO `t_sys_dict` VALUES (5,'gender','男','man','invalid',NULL,NULL),(6,'gender','女','woman','valid',NULL,NULL),(7,'province','山东','shandong','valid',NULL,1),(8,'province','河南','henan','valid',NULL,2),(9,'province','河北','hebei','valid',NULL,3);
/*!40000 ALTER TABLE `t_sys_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sys_xfile`
--

DROP TABLE IF EXISTS `t_sys_xfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sys_xfile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_realname` varchar(200) NOT NULL,
  `file_path` varchar(200) NOT NULL,
  `file_title` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `file_size` bigint(16) DEFAULT NULL,
  `user_id` bigint(16) DEFAULT NULL,
  `extension_name` varchar(16) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `is_deleted` char(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sys_xfile`
--

LOCK TABLES `t_sys_xfile` WRITE;
/*!40000 ALTER TABLE `t_sys_xfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sys_xfile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-05 17:52:55
