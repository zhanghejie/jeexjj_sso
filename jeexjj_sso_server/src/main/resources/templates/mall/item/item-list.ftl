<#--
/****************************************************
 * Description: 商品表的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>商品标题</th>
	        <th>商品卖点</th>
	        <th>商品价格</th>
	        <th>库存数量</th>
	        <th>售卖数量限制</th>
	        <th>商品图片</th>
	        <th>所属分类</th>
	        <th>状态</th>
	        <th>创建时间</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.title}
			</td>
			<td>
			    ${item.sellPoint}
			</td>
			<td>
			    ${item.price}
			</td>
			<td>
			    ${item.num}
			</td>
			<td>
			    ${item.limitNum}
			</td>
			<td>
				<img src="${item.image}" width="100px" height="120px"/>
			</td>
			<td>
			    ${item.cid}
			</td>
			<td>
				<#if item.status==1>
				正常
				<#else>
				下架
				</#if>
			</td>
			<td>
			    ${item.created?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<#--
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/item/input/${item.id}','修改商品表','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/item/delete/${item.id}','删除商品表？',false,{id:'${tabId}'});">删除</@button>
            </td>
            -->
		</tr>
		</#list>
	</tbody>
</@list>