<#--
/****************************************************
 * Description: t_sso_project的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-09 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>名称</th>
	        <th>编码</th>
	        <th>创建人</th>
	        <th>创建时间</th>
	        <th>登陆地址</th>
	        <th>状态</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.name}
			</td>
			<td>
			    ${item.code}
			</td>
			<td>
			    ${item.createUserName}
			</td>
			<td>
			    ${item.createDate?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
			    ${item.loginUrl}
			</td>
			<td>
			    <span class="label <#if item.status=XJJConstants.COMMON_STATUS_VALID>label-info</#if> arrowed-in arrowed-in-right">${XJJDict.getText(item.status)}</span>
			</td>
		</tr>
		</#list>
	</tbody>
</@list>