package com.xjj.sso.server.job;

import com.xjj.sso.server.cache.TicketCache;


public class TicketCacheJob {

	public void updateCache() {
		System.out.println("======调用定时更新ticket缓存任务=========");
		TicketCache.invalidateGrantingTicketByTime();
	}
}
