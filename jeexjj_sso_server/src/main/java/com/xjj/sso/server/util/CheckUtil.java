package com.xjj.sso.server.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckUtil {
	/**
	 * 验证邮箱地址是否正确
	 * 
	 * @param email
	 * @return
	 */
	private static String EMAIL_REG = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
	private static String MOBILE_REG = "^1[0-9]{10}$";
	public static boolean isEmail(String email) {
		boolean flag = false;
		if(!email.contains("@")){
			return flag;
		}
		try {
			//String check_bak = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			//String check_bak2 = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
			Pattern regex = Pattern.compile(EMAIL_REG);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}

		return flag;
	}

	/**
	 * 验证手机号码
	 * 
	 * @param mobiles
	 * @return [0-9]{5,9}
	 */
	public static boolean isMobile(String mobiles) {
		boolean flag = false;
		try {
			Pattern p = Pattern.compile(MOBILE_REG);
			Matcher m = p.matcher(mobiles);
			flag = m.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	
	
	public static boolean isMatching(String src,String des){

		String des1 = des.replace("*", "[\\w\\/]*");
		des1 = des1.replace("?", "\\w{1}");
		Pattern p = Pattern.compile(des1);
		Matcher m = p.matcher(src);
		return m.matches();
	}

	public static void main(String[] args) {
		
//		System.out.println(isMobile("18222222222"));
//		System.out.println(isMobile("11111111111"));
//		System.out.println(isMobile("10000000000"));
		
		
		String src = "/aa/dd/cc/xx/bb/cc";
		String des = "/*/bb/*";
		
		
		System.out.println(isMatching(src,des));
	}
}
