/****************************************************
 * Description: Service for t_sso_project
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-09 zhanghejie Create File
**************************************************/
package com.xjj.sso.project.service;
import com.xjj.framework.service.XjjService;
import com.xjj.sso.project.entity.ProjectEntity;

public interface ProjectService  extends XjjService<ProjectEntity>{
	
	public ProjectEntity getByCode(String code);
}
